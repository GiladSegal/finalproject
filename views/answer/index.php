<?php

use yii\helpers\Html;
use yii\grid\GridView;
use dosamigos\chartjs\ChartJs;
use app\models\answer;
use app\controllers\AnswerController;


/* @var $this yii\web\View */
/* @var $searchModel app\models\AnswerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Answers');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="answer-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]);  
    //echo $dataProvider->content;?>


<h1> <?php //Answer::countQuestions();
        //Answer::getAnswers(); ?> 
</h1>





<section id="loopGraphCreating "> 
    <div class="body-content">

        <div class="row">
            

<?php 
$numOfQuestions = Answer::countQuestions();
$answers = Answer::getAnswers();
$questionId = 0;

foreach($answers as $graphNum){
    ?>
    <div class="col-lg-4">
    <?php

    $numAnsPerQuestion=AnswerController::countNumberOfAnswers($answers,$questionId);
    
    echo "Question number ". $questionId;
    //create a graph
    echo ChartJs::widget([
    'type' => 'bar',
    'options' => [
        'height' => 400,
        'width' => 400
    ],
    'data' => [
        'labels' => (AnswerController::getNames($answers[$questionId])),
        'datasets' => [
                        [
                'label' => "My Second dataset",
                'backgroundColor' => "rgba(255,99,132,0.2)",
                'borderColor' => "rgba(255,99,132,1)",
                'pointBackgroundColor' => "rgba(255,99,132,1)",
                'pointBorderColor' => "#fff",
                'pointHoverBackgroundColor' => "#fff",
                'pointHoverBorderColor' => "rgba(255,99,132,1)",
                'data' => (AnswerController::getValues($answers[$questionId]))
                         ]
                     ]
             ]
                     ]);


    $questionId++;

    ?>
    </div>

    <?php
}



?>

</div>
</div>
</div>
</section>







<script>
    //console.log(<  implode(Answer::getAnswers()," ")?>);
</script>
    <p>
        <?= Html::a(Yii::t('app', 'Create Answer'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>






    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'content',
            'questionId',
            'created_at',
            'updated_at',
            //'created_by',
            //'updated_by',
            //'ownerId',
            //'organizationId',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

     

    
</div>
