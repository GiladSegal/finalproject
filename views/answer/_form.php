<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Answer;
use app\models\Question;


/* @var $this yii\web\View */
/* @var $model app\models\Answer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="answer-form">

    <?php $form = ActiveForm::begin(); 
    $userid =\Yii::$app->user->identity->id;
    //$numberOfQuestions = Question::countQuestions();
    $questions = Question::getQuestions();
    $labels = Answer::getFieldLabels();
    $questionNumber=0;//counter to get question number for loop
    
    foreach($questions as $question){
       //show the question
        echo $question["body"]."\n";
        echo $question["explenation"]."\n";
        echo $question["help"]."\n"."\n";
        //show the input area
        foreach($labels as $label){
            $questionNumber++;
            if($label=='questionId'){//for questionid
                ?>
                 <div id="hide">
                <?php
              echo $form->field($model, $label)->textInput(['readonly' => true, 'value' => $questionNumber]); 
                ?>
                </div>
                <?php
            }
            else if ($label=='ownerId'){//for userId
                ?>
                 <div id="hide">
                <?php
               echo $form->field($model, $label)->textInput(['readonly' => true, 'value' => $userid]);
                ?>
                 </div>
                 <?php
            }
            else{
                 ?>
                 <div>
                <?php
                    echo $form->field($model, $label)->textInput(['maxlength' => true]);
                    ?>
                 </div>
                 <?php
            }
             
         }

    }

?>



<script>
window.onload = function () { alert("Wizard!"); 
document.getElementById('hide').style.visibility = "hidden";}

</script>


<!--
    <?= $form->field($model, 'questionId')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'updated_by')->textInput() ?>

    <?= $form->field($model, 'ownerId')->textInput() ?>

    <?= $form->field($model, 'organizationId')->textInput() ?>
-->
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
