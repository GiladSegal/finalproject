<?php

use yii\db\Migration;

/**
 * Handles the creation of table `sector`.
 */
class m180104_180624_create_sector_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('sector', [
            'id' => $this->primaryKey(),
            'sectorValue' => $this->string(),
            'sectorOrgs' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('sector');
    }
}
