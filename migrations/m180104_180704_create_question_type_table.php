<?php

use yii\db\Migration;

/**
 * Handles the creation of table `question_type`.
 */
class m180104_180704_create_question_type_table extends Migration
{
    /**
     * @inheritdoc
     */
     public function up()
    {
        $this->createTable('questionType', [
            'id' => $this->primaryKey(),
            'questionTypeValue' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('question_type');
    }
}
