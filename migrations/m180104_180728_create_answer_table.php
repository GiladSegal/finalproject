<?php

use yii\db\Migration;

/**
 * Handles the creation of table `answer`.
 */
class m180104_180728_create_answer_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('answer', [
            'id' => $this->primaryKey(),
            'content' => $this->string(),
            'questionId' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            //'sectorId' => $this->integer(), already in question
            'ownerId' => $this->integer(),
            'organizationId'  => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('answer');
    }
}
