<?php

use yii\db\Migration;

/**
 * Handles the creation of table `organization`.
 */
class m180104_180741_create_organization_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('organization', [
            'id' => $this->primaryKey(),
            'phone' => $this->integer(),
            'email' => $this->text(),
            'city' => $this->string(),
            'address' => $this->string(),
            'name' => $this->string(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'sectorId' => $this->integer(),
            'ownerId' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('organization');
    }
}
