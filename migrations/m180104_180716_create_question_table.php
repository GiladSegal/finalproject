<?php

use yii\db\Migration;

/**
 * Handles the creation of table `question`.
 */
class m180104_180716_create_question_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('question', [
            'id' => $this->primaryKey(),
            'explenation' => $this->text(),
            'sectorId' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'status' => $this->string(),
            'show' => $this->boolean(),
            'body' => $this->text(),
            'type' => $this->integer(),
            'help' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('question');
    }
}
