<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_profile`.
 */
class m180104_180548_create_user_profile_table extends Migration
{
    /**
     * @inheritdoc
     */
      public function up()
    {
        $this->createTable('user_profile', [
            'id' => $this->primaryKey(),
            'avatar' => $this->string(),
            'ficFirstName' => $this->string(),
            'ficLastName' => $this->string(),
            'userId' => $this->integer(),
           

        ]);

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user_profile');
    }
}
