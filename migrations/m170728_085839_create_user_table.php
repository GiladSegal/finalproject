<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m170728_085839_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'username' => $this->string(),
            'password' => $this->string(),
            'auth_Key' => $this->string(),
            'firstname' => $this->string(),
            'lastname' => $this->string(),
            'phoneNumber' => $this->integer(),
            'address' => $this->string(),
            'profileId' => $this->integer(),

        ]);

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user');
    }
}
