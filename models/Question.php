<?php

namespace app\models;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

use Yii;

/**
 * This is the model class for table "question".
 *
 * @property int $id
 * @property string $explenation
 * @property int $sectorId
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 * @property string $status
 * @property int $show
 * @property string $body
 * @property int $type
 * @property string $help
 */
class Question extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'question';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['explenation', 'body'], 'string'],
            [['sectorId', 'created_at', 'updated_at', 'created_by', 'updated_by', 'show', 'type','category'], 'integer'],
            [['status', 'help'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'explenation' => Yii::t('app', 'Explenation'),
            'sectorId' => Yii::t('app', 'Sector ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'status' => Yii::t('app', 'Status'),
            'show' => Yii::t('app', 'Show'),
            'body' => Yii::t('app', 'Body'),
            'type' => Yii::t('app', 'Type'),
            'help' => Yii::t('app', 'Help'),
            'category' => Yii::t('app', 'category'),
            

        ];
    }

    /**
     * @inheritdoc
     * @return QuestionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new QuestionQuery(get_called_class());
    }

        public function behaviors()
    {
        return 
        [
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
             ],
                'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }
    
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }   

    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }



    
    public static function countQuestions(){//counts the number of questions in DB
        $count = Question::find()->all()->count();
        return $count;
    }

    public static function getQuestions(){
           return Question::find()->select(array('id'=>'Id','body'=>'body','explenation'=>'explenation','help'=>'help','type'=>'type','category'=>'category'))->where(['show'=>1])->asArray()->all();
        
    }

    //public static function getNumOfCategories(){
     //   $count = 
    //}


}