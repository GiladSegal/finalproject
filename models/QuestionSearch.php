<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\question;

/**
 * QuestionSearch represents the model behind the search form of `app\models\question`.
 */
class QuestionSearch extends question
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'sectorId', 'created_at', 'updated_at', 'created_by', 'updated_by', 'show', 'type'], 'integer'],
            [['explenation', 'status', 'body', 'help'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = question::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'sectorId' => $this->sectorId,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'show' => $this->show,
            'type' => $this->type,
        ]);

        $query->andFilterWhere(['like', 'explenation', $this->explenation])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'body', $this->body])
            ->andFilterWhere(['like', 'help', $this->help]);

        return $dataProvider;
    }
}
