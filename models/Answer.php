<?php

namespace app\models;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

use Yii;

/**
 * This is the model class for table "answer".
 *
 * @property int $id
 * @property string $content
 * @property int $questionId
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $ownerId
 * @property int $organizationId
 */
class Answer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'answer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['questionId', 'created_at', 'updated_at', 'created_by', 'updated_by', 'ownerId', 'organizationId'], 'integer'],
            [['content'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'content' => Yii::t('app', 'Content'),
            'questionId' => Yii::t('app', 'Question ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'ownerId' => Yii::t('app', 'Owner ID'),
            'organizationId' => Yii::t('app', 'Organization ID'),
        ];
    }

    /**
     * @inheritdoc
     * @return AnswerQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AnswerQuery(get_called_class());
    }


    public static function countQuestions(){//counts the number of questions in the array
         $count = Answer::find()->select('questionId')->distinct()->count();
        
//uncomment to test recieved data
        //echo $count;
        //echo "-------------";
        return $count;
    }

    public static function getAnswers(){
        $numOfQuestions = Answer::countQuestions();
        $answersSet = array();
        for ($question = 0; $question < $numOfQuestions; $question++){     //runs over all the unique questions
                $answersSet[]= Answer::find()->select(array('ownerId'=>'ownerId','content'=>'content'))->where(['questionId'=>$question])->asArray()->all();//  gets all the answers for a certain question
                
//uncomment all "//" to test recieved data
                //foreach($answersSet[$question] as $answer){
                 //   echo " - ";
                 //   echo implode($answer," ");

                //}
               // echo "end";
               
                
        }
        return $answersSet;
    }

    public static function getFieldLabels(){//returns the labels to present in the wizard
        $labels = array('ownerId', 'questionId', 'content');

        return $labels;
    }

    public function behaviors()
    {
        return 
        [
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                
             ],
                'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }
    
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }   

    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

}
